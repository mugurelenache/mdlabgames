---
layout: post
title: "Mercedes-Benz Car Configurator VR"
img: "/assets/img/mercedes.png"
vid: "https://youtube.com/embed/8VyY1mbsF2E?t=0"
---
VR showcase for Mercedes-Benz purposed for a showroom. Connected through a mobile application, the users are able to interact in different ways with the car and the experiences.
