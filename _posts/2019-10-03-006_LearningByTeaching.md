---
layout: post
title: "Learning by Teaching"
img: "/assets/img/learningbyteaching.jpg"
vid: "https://www.youtube.com/embed/uaHJE7kW-Cc"
---
The project teaches computer programming, physics, chemistry, history and biology to high-school students from the teacher's point of view, by going through the experiments in his daily adventures.
