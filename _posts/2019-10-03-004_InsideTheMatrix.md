---
layout: post
title: "Inside the Matrix VR"
img: "/assets/img/matrix.png"
vid: "https://www.youtube.com/embed/vEkXyjjhgSk"
---
A VR simulation trying to teach Computer Architecture & Processors from within the computer. It was created at Leeds Hack '18.
