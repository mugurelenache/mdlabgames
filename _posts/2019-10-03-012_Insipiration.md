---
layout: post
title: "Inspiration VR"
img: "/assets/img/archviz.jpg"
vid: "https://www.youtube.com/embed/zycQoMPvZTs"
---
This project was created in partnership with Collider Visuals, featuring a realistic ArchViz VR experience. We helped with the navigation and interaction system, as well as porting it to different VR platforms as: HTC Vive, GearVR, Oculus Rift.
